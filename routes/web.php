<?php

use App\Http\Controllers\AuthController;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/login');
});

Route::get('/login', [AuthController::class,"loginView"])->name('login');
Route::get('/register', [AuthController::class,"registerView"]);
Route::get('/forgot', [AuthController::class,"forgot"]);
Route::post('/do-login', [AuthController::class,"doLogin"]);
Route::post('/do-register', [AuthController::class,"doRegister"]);
Route::post('/do-forgot-password', [AuthController::class,"doForgotPassword"]);

Route::group(['middleware' =>Authenticate::class ], function () {
    Route::get('/dashboard', [AuthController::class,"dashboard"]);
    Route::get('/logout', [AuthController::class,"logout"]);
    Route::get('/profile', [AuthController::class,"profile"]);
    Route::post('/do-update-profile', [AuthController::class,"doUpdateProfile"]);
    Route::get('/password', [AuthController::class,"password"]);
    Route::post('/do-update-password', [AuthController::class,"doUpdatePassword"]);
    Route::get('/positions', 'App\Http\Controllers\PositionsController@index')->name('positions');
});
