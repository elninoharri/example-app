<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Putera',
                'email' => 'putera.harri@gmail.com',
                'gender' => 0,
                'dob' => '1992-06-15',
                'address' => 'Jl. Pademangan Timur',
                'email_verified_at' => NULL,
                'password' => '$2y$10$gT42NYnhF0YNNGC0rr8JXuzAiOXhnCkPrQ7N2e4sNFCjWiJPVxPR2',
                'remember_token' => NULL,
                'created_at' => '2022-07-29 06:53:46',
                'updated_at' => '2022-07-30 12:08:50',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Fahmi',
                'email' => 'fahmi@gmail.com',
                'gender' => 0,
                'dob' => '1988-07-20',
                'address' => 'Jl. Mampang Prapatan',
                'email_verified_at' => NULL,
                'password' => '$2y$10$aol.6w/VSOImm0GYT3xirOwglXN8SdA3UVv4iTJ9IjbRSk7SKrMf6',
                'remember_token' => NULL,
                'created_at' => '2022-08-01 03:37:29',
                'updated_at' => '2022-08-01 03:39:21',
            ),
        ));
        
        
    }
}