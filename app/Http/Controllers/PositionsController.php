<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Positions;
use App\Traits\PaginationTrait;

class PositionsController extends Controller
{
    use PaginationTrait;

    private $lang;
    private $master;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->master = new Positions($this->lang);
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $paramsList = $this->getParams();
        
        $mergeParams = array_merge($paramsList);
        
        $positions = $this->master->getPositions($mergeParams);
        
        $positions = $this->paginate($positions);
        $positions->withPath('');
        
        return view('positions', [
            'positions'=> $positions,
            'query' => $request->all()
        ]);
    }
}