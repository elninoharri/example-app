<?php


namespace App\Traits;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

trait PaginationTrait
{
    public function getParams($perPage = 10)
    {
        $default = ['limit' => $perPage, 'page' => 1];
        return array_merge($default, request()->all());
    }

    public function paginateArray($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function paginate($items, $perPage = 10, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $total = count($items);
        $currentpage = $page;
        $offset = ($currentpage * $perPage) - $perPage ;
        $itemstoshow = array_slice($items , $offset , $perPage);
        return new LengthAwarePaginator($itemstoshow ,$total ,$perPage);
    }

    public function paginateApi($data, $perPage = 10)
    {
        $meta = null;
        if (property_exists($data, 'meta')) {
            $meta = $data->meta->pagination ?? null;
        }

        if ($meta) {
            return new LengthAwarePaginator($data, $meta->total, $meta->per_page, $meta->current_page);
        }
        return new LengthAwarePaginator($data, 0, $perPage, 1);
    }

}
