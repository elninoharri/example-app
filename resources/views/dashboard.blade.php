<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Dashboard </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" />
  
  <script src="{{ url('assets/library/bootstrap.min.js') }}" type="text/javascript"
    charset="utf-8"></script>
  <style>
    .error {
      color: red !important
    }
    .dash{
      height: 400px;
      justify-content: center;
      align-items: center;
      font-size: 20px;
      font-weight: bold;
      display: flex;
      color:green;
      flex-direction: column;

    }
  </style>
</head>

<body class="antialiased">
  
    
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light justify-content-end">
      <li class="nav-item dropdown" style="list-style-type: none;">
        <a class="navbar-brand nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ url('assets/images/default-profile.jpg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
          @if(\Auth::check())
          Welcome, {{\Auth::user()->email}}
          @else
          @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Dashboard</a>
          <a class="dropdown-item" href="{{url('profile')}}">Update Profile</a>
          <a class="dropdown-item" href="{{url('password')}}">Update Password</a>
          <a class="dropdown-item" href="{{url('positions')}}">Positions Data</a>
          <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
        </div>
      </li>
    </nav>
    <br>
    <br>
    <div class="row">
      <div class="col-md-6 offset-md-3">
        
        <!-- Show any success message -->
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        @endif
        @if (\Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>{!! \Session::get('error') !!}</li>
                </ul>
            </div>
        @endif
      <!-- Show any success message -->

        <!-- Check user is logged in -->
        @if(\Auth::check())
          <div class='dash'>You are logged in as  : {{\Auth::user()->email}} ,  <a href="{{url('logout')}}"> Logout</a></div> 
        @else
        <div class='dash '>
          <div class='error'> You are not logged in  </div>
          <div>  <a href="{{url('login')}}">Login</a> | <a href="{{url('register')}}">Register</a> </div>
        </div>
        @endif
      <!-- Check user is logged in --> 
      </div>
    </div>
    
    <!-- credits -->
    <div class="text-center">
      <p>
        <a href="#" target="_top"></a>
      </p>
    </div>
  
    
</body>
</html>