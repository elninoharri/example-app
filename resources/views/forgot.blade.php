<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Forgot Password </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" />
  
  <script src="{{ url('assets/library/bootstrap.min.js') }}" type="text/javascript"
    charset="utf-8"></script>
  <style>
    .error {
      color: red
    }
  </style>
</head>
<br>
<body class="antialiased">
    
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" id="handleAjax" action="{{url('do-forgot-password')}}" name="postform">
                      <div class="form-group">
                          <label>Email</label>
                          <input type="email" name="email" class="form-control" />
                          @csrf
                      </div>
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
                      </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</body>

</html>