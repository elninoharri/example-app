<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Update Password </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" />
  
  <script src="{{ url('assets/library/bootstrap.min.js') }}" type="text/javascript"
    charset="utf-8"></script>
  <style>
    .error {
      color: red
    }
  </style>
</head>
<br>
<body class="antialiased">
    
    <nav class="navbar navbar-light bg-light justify-content-end">
      <li class="nav-item dropdown" style="list-style-type: none;">
        <a class="navbar-brand nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ url('assets/images/default-profile.jpg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
          @if(\Auth::check())
          Welcome, {{\Auth::user()->email}}
          @else
          @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{url('dashboard')}}">Dashboard</a>
          <a class="dropdown-item" href="{{url('profile')}}">Update Profile</a>
          <a class="dropdown-item" href="{{url('password')}}">Update Password</a>
          <a class="dropdown-item" href="{{url('positions')}}">Positions Data</a>
          <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
        </div>
      </li>
    </nav>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" id="handleAjax" action="{{url('do-update-password')}}" name="postform">
                      <div class="form-group">
                          <label>Password</label>
                          <input type="password" name="password"   class="form-control" />
                          @csrf
                      </div>
                      <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" />
                        </div>
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
                      </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</body>

</html>