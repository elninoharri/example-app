<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Profile </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" />
  
  <script src="{{ url('assets/library/bootstrap.min.js') }}" type="text/javascript" charset="utf-8"></script>
  <style>
    .error {
      color: red
    }
  </style>
</head>
<br>
<body class="antialiased">
    
    <nav class="navbar navbar-light bg-light justify-content-end">
      <li class="nav-item dropdown" style="list-style-type: none;">
        <a class="navbar-brand nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ url('assets/images/default-profile.jpg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
          @if(\Auth::check())
          Welcome, {{\Auth::user()->email}}
          @else
          @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{url('dashboard')}}">Dashboard</a>
          <a class="dropdown-item" href="{{url('profile')}}">Update Profile</a>
          <a class="dropdown-item" href="{{url('password')}}">Update Password</a>
          <a class="dropdown-item" href="{{url('positions')}}">Positions Data</a>
          <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
        </div>
      </li>
    </nav>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" id="handleAjax" action="{{url('do-update-profile')}}" name="postform">
                        <div class="form-group">
                            <label>Fullname</label>
                            <input type="text" name="name" id="name" value="{{ $user->name }}" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" id="email" value="{{ $user->email  }}" class="form-control" readonly/>
                        @csrf
                        </div>
                        <div class="form-group">
                            <label>DOB</label>
                            <input type="date" name="dob" id="dob" class="form-control" value="{{ $user->dob }}" />
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control" id="gender" name="gender">
                                <option>--Please Select--</option>
                                @if($user->gender == 0)
                                <option value="0" selected>Male</option>
                                @else
                                <option value="0">Male</option>
                                @endif
                                @if($user->gender == 1)
                                <option value="1" selected>Female</option>
                                @else
                                <option value="1">Female</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" id="address" name="address" rows="3">{{ $user->address ?? '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</body>

</html>