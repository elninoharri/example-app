<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Login Form </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
  <script type="text/javascript" src="index.js"></script>
  <style>
    .error {
      color: red
    }
  </style>
</head>
<body class="antialiased">
    <div class="container">
    <!-- main app container -->
        <br>
        <hr>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{!! \Session::get('error') !!}</li>
                        </ul>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" id="handleAjax" action="{{url('do-login')}}" name="postform">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" value="{{old('email')}}"  class="form-control" />
                        @csrf
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password"   class="form-control" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                    </div>
                </form>
            </div>
        </div>
        
        <!-- credits -->
        <div class="text-center">
            <p>
                <a href="{{url('register')}}">Create New Account</a> | <a href="{{url('forgot')}}">Forgot Password</a>
            </p>
        </div>
        <br>
        <hr>
    </div>
    
</body>

</html>